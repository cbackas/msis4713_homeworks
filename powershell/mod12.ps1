# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Mod12 HW Hash Tables

$global:eventHash = @{"Weezer" = @{"Jan 30; 2018" = @("Concrete Gold", "Etihad Stadium", "Melbourne, VIC");
        "Jun 16; 2018"                            = @("Montebello Rockfest 2018"; "Montebello Marina"; "Montebello; QC")
    };
    "Tenacious D"              = @{"May 06; 2018" = @("Shaky Knees Music Festival 2018"; "Central Park"; "Atlanta; GA");
        "Jun 16; 2018"               = @("Montebello Rockfest 2018"; "Montebello Marina"; "Montebello; QC")
    };
    "Lamb of God"              = @{"Jun 09; 2018" = @("Final World Tour= North America 2018"; "Keybank Pavilion"; "Burgettstown; PA");
        "Jun 16; 2018"               = @("Montebello Rockfest 2018"; "Montebello Marina"; "Montebello; QC")
    };
    "Ed Sheeran"               = @{"Mar 10; 2018" = @("Ed Sheeran with Missy Higgins"; "Etihad Stadium"; "Melbourne; VIC") };
    "Cold War Kids"            = @{"Jun 02; 2018" = @("XFEST 2018"; "Keybank Pavilion"; "Burgettstown; PA") };
    "Steel Panther"            = @{"Oct 21; 2017" = @("Aftershock"; "Discovery Park"; "Sacramento; CA") }
};

# inserts a concert into the dictionary
function AddEvent([String]$bandName, [String]$eventName, [String]$date, [String]$venue, [String]$location) {
    if (!$global:eventHash.ContainsKey($bandName)) {
        $global:eventHash[$bandName] = @{ };
    }

    $global:eventHash[$bandName][$date] = @($eventName; $venue; $location);
}

# for each band, for each concert, list info
function PrintEvents() {
    foreach ($band in $global:eventHash.Keys) {
        Write-Host($band);

        $dates = $global:eventHash[$band].Keys;
        foreach ($d in $dates) {
            Write-Host("    $d");
            $infoLines = $global:eventHash[$band][$d];
            foreach ($infoLine in $infoLines) {
                Write-Host("        $infoLine");
            }
        }
        Write-Host("");
    }
}

cls
# Starts here. Prints existing concert entries
PrintEvents;

# exists program if user doesn't opt to add events
Write-Host("Enter Y if you would like to add more events:");
$addPrompt = Read-Host -Prompt "Y/N";
cls
if ($addPrompt -ne "y") {
    break;
} else {
    While ($true) {
        # prompts for concert info and sends it to the add_concert function
        $inputBandName = Read-Host -Prompt "Artist or Band";
        $inputEventName = Read-Host -Prompt "Event";
        $inputDate = Read-Host -Prompt "Date";
        $inputVenue = Read-Host -Prompt "Venue";
        $inputLocation = Read-Host -Prompt "Location";

        AddEvent -bandName $inputBandName -eventName $inputEventName -date $inputDate -venue $inputVenue -location $inputLocation;
        cls

        # if user stops the add-loop then print bands and break out
        Write-Host("Press Q to stop, anything else to continue.");
        $continuePrompt = Read-Host -Prompt "Q?";
        if ($continuePrompt -ne 'q') {
            cls
            PrintEvents
            continue;
        } else {
            break;
        }
    }
}
cls