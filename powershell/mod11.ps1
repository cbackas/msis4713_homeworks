# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Mod11 HW Strings

$lyricString = ("Quisiera:Ayer:cambiarle:conocí:el:un:final" +
               ":cielo:al:sin:cuento|Las:sol|Y:barras:un:y" +
               ":hombre:los:sin:tragos:suelo|Un:han:santo:" +
               "sido:en:testigo|De:prision|Y:el:una:dolor:" +
               "canción:que:triste:me:sin:causaste:dueño|Y:" +
               "y:conocí:to':tus:lo:ojos:que:negros|Y:hiciste" +
               ":ahora:conmigo|Un:sí:infeliz:que:en:no:el:" +
               "puedo:amor,:vivir:que:sin:aun:ellos:no:yo|" +
               "Le:te:pido:supera|Que:al:ahora:cielo:camina" +
               ":solo:solo:un:sin:deseo|Que:nadie:en:por:tus" +
               ":todas:ojos:las:yo:aceras|Preguntándole:pueda" +
               ":a:vivir|He:Dios:recorrido:si:el:en:mundo:verdad" +
               ":entero|te:el:vengo:amor:a:existe|:decir|")

$listOne = @()
$listTwo = @()
$sharedWords = @()

cls

# converts allStrings to a list
$lyricArray = $lyricString.Split(":")

# loops and unzipper merges the two songs into listOne and listTwo
for ($i = 0; $i -lt $lyricArray.Length; $i++) {
    $test = $i % 2 -eq 0
    if (-not $test) {
        $listOne += $lyricArray[$i]
    } else {
        $listTwo += $lyricArray[$i]
    }
}

# turns arrays into strings with spaces
$songOne = ''
$songTwo = ''
foreach ($word in $listOne) {
    $songOne += $word
    $songOne += ' '
}
foreach ($word in $listTwo) {
    $songTwo += $word
    $songTwo += ' '
}
$songOne = $songOne.Replace("|","`n")
$songTwo = $songTwo.Replace("|","`n")

# prints the two sets of lyrics
Write-Host("----- SONG ONE -----")
Write-Host $songOne
Write-Host("----- SONG TWO -----")
Write-Host $songTWo
Write-Host

# finds matching words in the songs then prints them
Write-Host("Words that are in both songs:")
foreach ($word in $listOne) {
    # check if the word is in the other list
    $boolInListTwo = $listTwo.Contains($word)
    # check if we've already found that word
    $boolNotUsed = -not $sharedWords.Contains($word)

    if ($boolInListTwo -and $boolNotUsed) {
        $sharedWords += $word
        Write-Host($word)
    }
}