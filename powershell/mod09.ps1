# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Mod09 HW error checking

# calculates the math that we need
function Calculate-Math([int]$intInput) {
    $intOutput = [Math]::Pow($intInput,$intInput) + $intInput
    return $intOutput
}

# big loop (so we don't keep saying "please 
while ($true) {
    Write-Host('Please enter an integer between 1 and 10:')
    $userInput = Read-Host -Prompt 'number'
    # tries to make $userInput a int, fails and reloops if not
    try {
        $userInput = $userInput/1
    } catch {
        Write-Host('That is not an integer. Please try again.')
        continue
    }

    # checks that input is in range
    if ($userInput -gt 10 -or $userInput -lt 1) {
        Write-Host('Integer out of bounds. Please try again.')
    } else {
        $result = Calculate-Math -intInput $userInput
        Write-Host($result)

        $endPrompt = Read-Host -Prompt 'Press q to exit, anything else to go again'
        if ($endPrompt -eq 'q' -or $endPrompt -eq 'Q') {
            break # break out when they're done
        } else {
            clear
            continue # go for another round
        }
    }
}