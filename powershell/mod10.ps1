# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Mod10 HW Arrays

cls
$arrayMain = @()

# puts an item randomly within the 10 length array
function randomInsert($newItem) {
    $randomInt = Get-Random -Minimum 0 -Maximum 9
    $arrayMain[$randomInt] = $newItem
}

# prompts for list filling, 10 items
Write-Host("Please enter 10 numbers or words:")
for ($i=0; $i -lt 10; $i++) {
    $inputListItem = Read-Host -Prompt "Input"

    # makes sure it interprets numbers and numbers and strings as strings
    try {
        $arrayMain += $inputListItem/1
    } catch {
        $arrayMain += $inputListItem
    }
}

cls
# check to make sure the list has 10 items
$isFull = $arrayMain.Length -eq 10
Write-Host("This list has 10 items. $isFull`r`n")

# print list
Write-Host("Here's the list:")
Write-Host("$arrayMain`r`n")

# swap first and last item and print
$first = $arrayMain[0]
$last = $arrayMain[9]
$arrayMain[0] = $last
$arrayMain[9] = $first
Write-Host("This is the list after swapping the first and last items:")
Write-Host("$arrayMain`r`n")

# print first and last 3 in the main list
$firstThree = @($arrayMain[0],$arrayMain[1],$arrayMain[2])
$lastThree = @($arrayMain[7],$arrayMain[8],$arrayMain[9])
Write-Host("First Three: $firstThree")
Write-Host("Last Three: $lastThree`r`n")

# loop and print
Write-Host("These are the individual items in my list:")
foreach ($i in $arrayMain) {
    Write-Host $i
}
Write-Host

# check if theres a cat, print return
if ($arrayMain.Contains("cat")) {
    Write-Host("There is a cat in my list.`r`n")
} else {
    Write-Host("There is not a cat in my list.`r`n")
}

# prompt for marvel character
Write-Host("Please enter the name of a marvel character")
$inputCharacter = Read-Host -Prompt "Character"
randomInsert -newItem $inputCharacter
Write-Host

# tell user where the marvel character is (probably snapped)
$characterIndex = $arrayMain.IndexOf($inputCharacter)
Write-Host("$inputCharacter is at index $characterIndex")
Write-Host("$arrayMain`r`n")

# make a list of only integers
$intList = @()
foreach ($i in $arrayMain) {
    try {
        $intList += $i/1
    } catch {
        continue
    }
}
# sort and print
$intList = $intList | sort
Write-Host("These are the integers from the list:")
Write-Host $intList