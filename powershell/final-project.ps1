# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Powershell Final Project

$global:path_desktop = "C:\Users\Burkman\Desktop\";
$global:path_clientData = $global:path_desktop + "Client Data\";
$global:path_data = $global:path_clientData + "Data\";
$global:path_originalFiles = $global:path_data + "Original Files\";

$global:dataCache = [PSCustomObject]$null;

<# 
Branches from the main menu 
#>
function runReportBranch() {
    Write-Host;
    Write-Host("1) List client names, company and salary by company, state or city");
    Write-Host("2) Display the client names and salaries who are in the top X% of salaries");
    Write-Host("3) Display number of clients in age brackets");
    Write-Host("4) Display the average salary by company or by state");
    while ($true) {
        $reportChoice = Read-Host -Prompt "selection";
        if (!@('1','2','3','4','q').Contains($reportChoice)) {
            Write-Host("[Error] Input not recognized");
            continue;
        } else {
            if ($reportChoice -eq "q") {
                break;
            }

            $prelimCache = @($global:dataCache);

            # checks that theres nothing in the DB and kicks you back
            if ($null -eq $prelimCache[0]) {
                Write-Host ("There are no clients saved in the db...");
                Read-Host -Prompt "Any key to continue";
                break;
            }

            # excludes all the rejected clients from the reports
            $innerCache = @([PSCustomObject]$null);
            foreach ($client in $prelimCache) {
                $clientName = [String]$client."Full Name";
                if ($clientName.StartsWith("*REJECT*")) {
                    continue;
                } else {
                    $innerCache += $client;
                }
            }

            $output = @("",""); # output array to be used for all the choices
            if ($reportChoice -eq "1") {
                # List client names, company and salary by company, state or city.

                while ($true) {
                    Write-Host("Group by what?");
                    $groupType = Read-Host -Prompt "company/state/city".ToLower();
                    if (!@("company","state","city","q").Contains($groupType)) {
                        Write-Host("[Error] Input not recognized");
                        continue;
                    }

                    if ($groupType -eq "q") {
                        break;
                    }

                    # makes $sortBy equal to col headers
                    $sortBy = $groupType;
                    if (@("state","city").Contains($groupType)) {
                        $sortBy = "c" + $groupType;
                    }

                    # sorts clients by the sort type
                    $sortedClients = $innerCache | Sort-Object $sortBy;

                    # build output
                    $output[0] = "clientsby$groupType";
                    $output[1] = "--Clients Sorted By $groupType--`n";

                    $lastGroup = "";
                    foreach ($client in $sortedClients) {
                        $name = $client.'Full Name';
                        $company = $client.Company;
                        $salary = "`$" + $client.Salary;

                        # prints different headers for state and city groupings
                        $currGroup = $client.$sortBy;
                        if ($currGroup -ne $lastGroup) {
                            $output[1] += "$currGroup`n";
                            $lastGroup = $currGroup;
                        }

                        $output[1] += "  - $name ($company) $salary`n";
                    }

                    printReport -output $output;
                    break; # End of report 1
                }
            } elseif ($reportChoice -eq "2") {
                # Display the client names and salaries who are in the top X% of salaries.
                # Prompt the user for X% and include that information in your report header.

                Write-Host("Show clients in the top ? % of salaries:");
                while ($true) {
                    $percentagePrompt = Read-Host -Prompt "percentage";

                    if ($percentagePrompt -eq "q") {
                        break;
                    }

                    # try to interpret the input as a number
                    $percentage = 0
                    try {
                        $percentage = $percentagePrompt/1
                    } catch {
                        Write-Host("[Error] Couldn't parse percentage... Don't include %");
                        continue;
                    }

                    # simplifies the client cache down to the top x% of clients
                    $topClients = $innerCache | Sort-Object "Salary" -Descending | Select-Object -First ($innerCache.Count*($percentage/100));

                    $output[0] = "top" + $percentage + "percent";
                    $output[1] = "--Clients with top $percentage% of salaries--`n"

                    # builds the rest of the output
                    foreach ($client in $topClients) {
                        $name = $client.'Full Name';
                        $salary = "`$" + $client.Salary;
                        $output[1] += "$name - $salary`n";
                    }

                    printReport -output $output;
                    break; # end of report 2
                }
            } elseif ($reportChoice -eq "3") {
                # Display a list of the number of clients who are in the age brackets:
                # younger than 20, between 20 and 30 years old, 30 and 40, 40 and 50, and over 50.

                # counters for the age brackets
                $bracket1 = 0;
                $bracket2 = 0;
                $bracket3 = 0;
                $bracket4 = 0;
                $bracket5 = 0;

                $todayDate = Get-Date;
                foreach ($client in $innerCache) {
                    # Does a calculation to get age in years
                    $dobDate = [DateTime]$client.dob;
                    $age = [math]::Round((New-TimeSpan -Start $dobDate -End $todayDate).TotalDays/365,0);

                    # iterate counters for the brackets
                    if ($age -lt 20) {
                        $bracket1++;
                    } elseif ($age -ge 20 -and $age -lt 30) {
                        $bracket2++;
                    } elseif ($age -ge 30 -and $age -lt 40) {
                        $bracket3++;
                    } elseif ($age -ge 40 -and $age -lt 50) {
                        $bracket4++;
                    } elseif ($age -ge 50) {
                        $bracket5++;
                    }
                }

                # build output
                $output[0] = "agebrackets" + $todayDate.ToShortDateString();
                $output[1] = "--Age Brackets--`n";
                $output[1] += "Under 20: $bracket1`n";
                $output[1] += "20 to 30: $bracket2`n";
                $output[1] += "30 to 40: $bracket3`n";
                $output[1] += "40 to 50: $bracket4`n";
                $output[1] += "50 plus:  $bracket5`n";

                printReport -output $output;
            } elseif ($reportChoice -eq "4") {
                # Display the average salary by company, by state, or by both (each state by company)
                
                while ($true) {
                    Write-Host("What do you want to group average salary by?");
                    $averagePrompt = Read-Host -Prompt("company/state/both");
                    if (!@("company","state","both","q").Contains($averagePrompt)) {
                        Write-Host("[Error] Input not recognized");
                        continue;
                    }

                    if ($averagePrompt -eq "q") {
                        break;
                    }

                    # build output
                    $output[0] = "avgSalary-$averagePrompt";
                    $output[1] = "-- Average Salalary by $averagePrompt";

                    # depending on chosen groupings, groups and calculates averages
                    if ($averagePrompt -eq "company") {
                        $companyMap = groupByThing -clientSource $innerCache -groupType "company";
                        $companyAverage = averageSalaries -groupMap $companyMap;
                        
                        # builds the rest of the output
                        foreach ($company in $companyAverage.Keys) {
                            $avgSal = "$" + $companyAverage[$company];
                            $output[1] += "`n$company - $avgSal";
                        }
                    } elseif ($averagePrompt -eq "state") {
                        $stateMap = groupByThing -clientSource $innerCache -groupType "cstate";
                        $stateAverage = averageSalaries -groupMap $stateMap;
                        
                        # builds the rest of the output
                        foreach ($state in $stateAverage.Keys) {
                            $avgSal = "$" + $stateAverage[$state];
                            $output[1] += "`n$state - $avgSal";
                        }
                    } elseif ($averagePrompt -eq "both") {
                        # builds the rest of the output
                        $output[1] = "Didn't have time to figure out the more complex average with this type of hashtable...";
                    }


                    printReport -output $output;
                    break; # end of report 4
                }
            }
            Read-Host -Prompt "Press any key to continue...";
            break; # breaks out of whole report loop
        }
    }
}

function runPhoneBranch() {
    Write-Host;
    Write-Host("1) Lookup a specific person`n2) All in a specific state`n3) All in a specific company`n4) All in a specific region");
    while ($true) {
        $phoneChoice = Read-Host -Prompt "selection";
        if (!@("1","2","3","4","q").Contains($phoneChoice)) {
            continue;
        }

        if ($phoneChoice -eq "q") {
            break;
        }

        $prelimCache = @($global:dataCache);

        # checks that theres nothing in the DB and kicks you back
        if ($null -eq $prelimCache[0]) {
            Write-Host ("There are no clients saved in the db...");
            Read-Host -Prompt "Any key to continue";
            break;
        }

        # excludes all the rejected clients from the reports
        $innerCache = @([PSCustomObject]$null);
        foreach ($client in $prelimCache) {
            $clientName = [String]$client."Full Name";
            if ($clientName.StartsWith("*REJECT*")) {
                continue;
            } else {
                $innerCache += $client;
            }
        }

        if ($phoneChoice -eq "1") {
            # search a user with a search term
            while ($true) {
                Write-Host("`nEnter your search for client name: (q to cancel)");
                $inputName = Read-Host -Prompt "search";

                if ($inputName -eq "q") {
                    break;
                }

                $searchResult = searchForClient -searchPool $innerCache -nameSearch $inputName;
                if ($null -eq $searchResult) {
                    Write-Host("There were no clients found with names containing $inputName.");
                    continue;
                } else {
                    Write-Host;
                    Write-Host $searchResult."Full Name";
                    Write-Host $searchResult."company";
                    Write-Host $searchResult."CPhone1";
                    Write-Host $searchResult."caddress";
                }
                break; # end of phone dir 1
            }

        } elseif ($phoneChoice -eq "2") {
            # Show all clients in a specific state
            Write-Host("Show all clients in what state? (state code or 'listall')");
            while ($true) {
                $statePrompt = Read-Host -Prompt "state";
                $statePrompt = $statePrompt.ToUpper();

                if ($statePrompt -eq "q") {
                    break;
                }

                # if user says "listall" then show them a list of the available states
                if ($statePrompt -eq "listall") {
                    $stateList = $innerCache."cstate" | Sort-Object -Unique;
                    $stateList = $stateList -join ", ";
                    Write-Host("Stored States: $stateList");
                    continue;
                }

                if (!$innerCache."cstate".Contains($statePrompt)) {
                    Write-Host("[Error] State not found. Try again.");
                    continue;
                }

                $stateMap = groupByThing -clientSource $innerCache -groupType "cstate";
                
                Write-Host("`n[PhoneDir] Clients in $statePrompt`:");
                foreach ($client in $stateMap[$statePrompt]) {
                    # ignores any weird empty clients
                    # trims out spaces in the empty client and does an empty check
                    $clientName = [String]$client."Full Name";
                    $clientName = $clientName.Trim();
                    if ($clientName -eq "") {
                        continue; 
                    }
                    Write-Host;
                    Write-Host $client."Full Name";
                    Write-Host $client."company";
                    Write-Host $client."CPhone1";
                    Write-Host $client."caddress";
                }
                break; # end of phone dir 2
            }
        } elseif ($phoneChoice -eq "3") {
            # Show all clients in a specific company
            Write-Host("Show all clients in what company? (or 'listall')");
            while ($true) {
                $companyPrompt = Read-Host -Prompt "company";
                $companyPrompt = (Get-Culture).TextInfo.ToTitleCase($companyPrompt.ToLower())

                if ($companyPrompt -eq "q") {
                    break;
                }

                # if user says "listall" then show them a list of the available companies
                if ($companyPrompt -eq "listall") {
                    $companyList = $innerCache."company" | Sort-Object -Unique;
                    $companyList = $companyList -join ", ";
                    Write-Host("Stored Companies: $companyList");
                    continue;
                }

                if (!$innerCache."company".Contains($companyPrompt)) {
                    Write-Host("[Error] Company not found. Try again.");
                    continue;
                }

                $companyMap = groupByThing -clientSource $innerCache -groupType "company";
                
                Write-Host("`n[PhoneDir] Clients in $companyPrompt`:");
                foreach ($client in $companyMap[$companyPrompt]) {
                    # ignores any weird empty clients
                    # trims out spaces in the empty client and does an empty check
                    $clientName = [String]$client."Full Name";
                    $clientName = $clientName.Trim();
                    if ($clientName -eq "") {
                        continue; 
                    }
                    Write-Host;
                    Write-Host $client."Full Name";
                    Write-Host $client."company";
                    Write-Host $client."CPhone1";
                    Write-Host $client."caddress";
                }
                break; # end of phone dir 3
            }
        } elseif ($phoneChoice -eq "4") {
            # show all clients in a specific region
            Write-Host("Show all clients in what region? (or 'listall')");
            while ($true) {
                $regionPrompt = Read-Host -Prompt "region";
                
                if ($regionPrompt -eq "q") {
                    break;
                }

                $regions = @{"New England"=@('CT', 'ME', 'MA', 'MH', 'RI', 'VT');
                "Mid-Atlantic"=@('NJ', 'NY', 'PA');
                "East North Central"=@('IL', 'OH', 'IN', 'WI', 'MI');
                "West North Central"=@('ND', 'MN', 'SD', 'NE', 'KS', 'MO', 'IA');
                "Mountain"=@('MT', 'MY', 'CO', 'MN', 'AZ', 'UT', 'NV', 'ID');
                "Pacific"=@('WA', 'OR', 'CA');
                "West South Central"=@('TX', 'LA', 'AR', 'OK');
                "East South Central"=@('MS', 'AL', 'TN', 'KY');
                "South Atlantic"=@('FL', 'GA', 'SC', 'NC', 'VA', 'WV', 'MD', 'DE');}

                $regionList = $regions.Keys;

                # if user says "listall" then show them a list of the available companies
                if ($regionPrompt -eq "listall") {
                    $printList = $regionList -join ", ";
                    Write-Host("Avalable Regions: $printList");
                    continue;
                }

                if (!$regionList.Contains($regionPrompt)) {
                    Write-Host("[Error] Region not found. Try again.");
                    continue;
                }

                $qualifyingStates = $regions[$regionPrompt];

                $stateMap = groupByThing -clientSource $innerCache -groupType "cstate";

                Write-Host("`n[PhoneDir] Clients in $regionPrompt`:");
                foreach ($state in $stateMap.Keys) {
                    if ($qualifyingStates.Contains($state)) {
                        foreach ($client in $stateMap[$state]) {
                            # ignores any weird empty clients
                            # trims out spaces in the empty client and does an empty check
                            $clientName = [String]$client."Full Name";
                            $clientName = $clientName.Trim();
                            if ($clientName -eq "") {
                                continue; 
                            }

                            Write-Host;
                            Write-Host $client."Full Name";
                            Write-Host $client."company";
                            Write-Host $client."CPhone1";
                            Write-Host $client."caddress";
                        }
                    }
                }

                break; # end of phone dir 4
            }
        }
        Write-Host;
        Read-Host -Prompt "Press any key to continue...";
        break; # break out of phone branch
    }
}

function runImportBranch() {
    Write-Host;
    while ($true) {
        Write-Host("Enter the name and extension of source data.");
        $inputFile = Read-Host -Prompt "File";

        # check for quit
        if ($inputFile -eq "q") {
            break;
        } else {
            Set-Location($global:path_desktop);
            # tries to set $file to a read file, nulls if it can't
            $file = Get-Item($inputFile) -ErrorAction Ignore; # try/catch apparently doesn't work for this error?

            # check if file exists and that its a CSV
            if ($null -eq $file) {
                Write-Host("No file by that name was found.");
                continue;
            } elseif (!($file.Extension -eq ".csv")) {
                Write-Host("That isn't a csv file.");
                continue;
            }

            $importedCSV = Import-Csv -path $file.FullName;
            importIntoMaster -newCSV $importedCSV;
            
            Set-Location($global:path_originalFiles);
            try {
                #try to compress and delete the csv
                Compress-Archive -Path $file.FullName -DestinationPath $file.BaseName;
                Remove-Item -Path $file.FullName;
                Write-Host("[Import] Archived " + $file.BaseName + ".csv");
            } catch {
                # if it can't create the new zip just try to update the existing one
                try {
                    Compress-Archive -Path $file.FullName -Update -DestinationPath $file.BaseName;
                    Remove-Item -Path $file.FullName;
                    Write-Host("[Import] Archived " + $file.BaseName + ".csv");
                } catch {
                    # if it can't update the existing one then just give up
                    Write-Host("[Import] Failed to archive " + $file.BaseName + ".csv");
                }
            }

            break;
        }
    }
}

function runDeleteBranch() {
    Write-Host; 

    $prelimCache = @($global:dataCache);

    # checks that theres nothing in the DB and kicks you back
    if ($null -eq $prelimCache[0]) {
        Write-Host ("There are no clients saved in the db...");
        Read-Host -Prompt "Any key to continue";
        return;
    }

    # excludes all the rejected clients from the reports
    $innerCache = @([PSCustomObject]$null);
    foreach ($client in $prelimCache) {
        $clientName = [String]$client."Full Name";
        if ($clientName.StartsWith("*REJECT*")) {
            continue;
        } else {
            $innerCache += $client;
        }
    }

    Write-Host("To delete a client, enter your search for a client name:");
    while ($true) {
        $nameSearch = Read-Host -Prompt "search";
        
        if ($nameSearch -eq "q") {
            break;
        }

        $searchResult = searchForClient -searchPool $innerCache -nameSearch $nameSearch;

        if ($null -eq $searchResult) {
            Write-Host("No clients were found with the search term $nameSearch.");
            continue;
        }

        $name = $searchResult."Full Name";
        $dob = $searchResult."dob";

        Write-Host("[Delete] Are you sure you'd like to delete $name?");
        while ($true) {
            $deletePrompt = Read-Host -Prompt "y/n";

            if (!@("y","n","q").Contains($deletePrompt)) {
                continue;
            }

            if ($deletePrompt -eq "q") {
                break;
            }

            if ($deletePrompt -eq "y") {
                rejectClient -searchPool $innerCache -clientName $name -clientDOB $dob;
            } elseif ($deletePrompt -eq "n") {
                Write-Host("[Delete] Aborted delete of $name");
            }
            break;
        }

        break; # end of delete
    }
}

<#
Functions
#>

# makes sure the paths we need to use along the way
function initPaths() {
    $allPaths = @($global:path_clientData, $global:path_data, $global:path_originalFiles);
    foreach ($p in $allPaths) {
        $isPath = Test-Path -Path $p;
        if (!$isPath) {
            # creates the directory if it doesn't exist
            New-Item -ItemType "directory" -Path $p | out-Null;
        }
    }
}

# takes in an imported CSV and updates stored data
function importIntoMaster([PSCustomObject]$newCSV) {
    $acceptCount = 0;
    $rejectCount = 0;

    $innerCache = @($global:dataCache);
    foreach ($newClient in $newCSV) {
        # if innerCache is null (it has no clients) don't check for dupes just add them
        if ($null -eq $innerCache.'Full Name') {
            $innerCache += $newClient;
            continue;
        }

        $name = $newClient.'Full Name';
        $checkName = $innerCache.'Full Name'.Contains($name);
        $rejectName = "*REJECT*" + $name;
        $checkRejectName = $innerCache.'Full Name'.Contains($rejectName);
        if ($checkName -or $checkRejectName) {
            #prelim check for their name on the list before doing full match
            $clientExists = checkClientMatch -checkList $innerCache -name $name -dob $newClient.'dob';
            if ($clientExists) {
                # someone with that name and dob is already stored, skip
                $rejectCount++;
                continue;
            }
        }
        $acceptCount++;
        $innerCache += $newClient;
    }

    Write-Host("[Import] Imported ($acceptCount) clients.");
    Write-Host("[Import] Skipped ($rejectCount) clients.");

    # update cache and update CSV
    $global:dataCache = $innerCache
    updateMasterCSV;
}

# writes the current cache contents to the master CSV
function updateMasterCSV() {
    Set-Location -Path $global:path_data;
    # Exports all the cached data to the master CSV, suppresses a meaningless error on first file creation
    $global:dataCache | Export-Csv -Path "masterlist.csv" -NoTypeInformation -ErrorAction Ignore;
}

# a more thorough check for a full client match
function checkClientMatch([PSCustomObject]$checkList, [String]$name, [String]$dob) {
    foreach ($client in $checkList) {
        $nameCheck = $client.'Full Name' -eq $name;
        $rejectNameCheck = $client.'Full Name' -eq ("*REJECT*" + $name);
        $dobCheck = $client.'DOB' -eq $dob;
        if (($nameCheck -or $rejectNameCheck) -and ($dobCheck)) {
            return $true;
        }
    }
    return $false; # if it doesn't find a match, always returns false
}

# tries to read the master CSV and update the cache with that info
function readMasterToCache() {
    Set-Location -Path $global:path_data;
    $file = Get-Item("masterlist.csv") -ErrorAction Ignore; # try/catch apparently doesn't work for this error?
    if (!$null -eq $file) {
        # if the file isn't null then read it
        $global:dataCache = Import-Csv -path $file.FullName;
    } # if its null then whatever who needs it anyway (we'll create it)
}

# takes in a 2 item output array (fileName and reportContent) and handles report printing
function printReport([Array]$output) {
    $fileName = $output[0] + ".txt";
    $reportContent = $output[1];
    Write-Host;
    Write-Host("Print to screen, file, or both?");
    while ($true) {
        $outputPrompt = Read-Host -Prompt "screen/file/both".ToLower();

        if (!@("screen","file","both","q").Contains($outputPrompt)) {
            continue;
        }

        if ($outputPrompt -eq "q") {
            break;
        }

        # if its going to print to the screen, do that first
        # so the file output message will be below it
        if ($outputPrompt -eq "screen" -or $outputPrompt -eq "both") {
            Write-Host;
            Write-Host($reportContent);
        }
        if ($outputPrompt -eq "file" -or $outputPrompt -eq "both") {
            Set-Location $global:path_clientData;
            $outputFile = Get-Item($fileName) -ErrorAction Ignore;
            if ($null -eq $outputFIle) { # file doesnt already exist
                # try making a new file
                try {
                    New-Item -ItemType "File" -Name $fileName -Value $reportContent | Out-Null;
                    Write-Host("`n[Report] Report saved to $fileName");
                } catch {
                    Write-Host("`n[Error] Error saving report to $fileName");
                }
            } else { # file already exists
                while ($true) {
                    Write-Host("`n$fileName already exists. Overwrite?");
                    $overwritePrompt = Read-Host -Prompt "y/n".ToLower();
                    if (!@("y","n","q").Contains($overwritePrompt)) {
                        continue;
                    }

                    if ($overwritePrompt -eq "q") {
                        break;
                    }

                    if ($overwritePrompt -eq "y") {
                        # Delete existing file and create a new one
                        try {
                            $outputFile.Delete();
                            New-Item -ItemType "File" -Name $fileName -Value $reportContent | Out-Null;
                            Write-Host("`n[Report] Report saved to $fileName");
                        } catch {
                            Write-Host("`n[Error] Error saving report to $fileName");
                        }
                    } elseif ($overwritePrompt -eq "n") {
                        Write-Host("`n[Report] Aborting file overwrite.");
                    }
                    break; # end of file overwrite section
                }
            }
        }
        break; # break out of print loop - returns to the end of the reports section
    }
}

# takes in the cache and a group type (company or state) and groups the clients by that
function groupByThing([PSCustomObject]$clientSource, [String]$groupType) {
    $groupMap = @{};
    foreach ($client in $clientSource) {
        $group = $client.$groupType;
        $innerList = @($groupMap[$group]);
        $innerList += $client;
        $groupMap[$group] = $innerList;
    }
    return $groupMap;
}

# averages all the salaries given a map of companies/states
function averageSalaries([Hashtable]$groupMap) {
    $companyAverage = @{};
    foreach ($company in $groupMap.Keys) {
        $clientList = $groupMap[$company];
        $sum = 0.0;
        foreach ($salary in $clientList.salary) {
            $sum += $salary;
        }
        $average = [math]::Round($sum/$clientList.salary.Count,2);
        $companyAverage[$company] = $average;
    }
    return $companyAverage;
}

# given an input, performs an interactive search for the client
function searchForClient([PSCustomObject]$searchPool, [String]$nameSearch) {
    $nameSearch = $nameSearch.ToLower();
    $results = @();
    foreach ($client in $searchPool) {
        $contestantName = [String]$client."Full Name";
        $contestantName = $contestantName.ToLower();
        if ($contestantName.Contains($nameSearch)) {
            $results += $client;
        }
    }

    $resultCount = $results.Count;
    if ($resultCount -eq 1) {
        return $results[0];
    } elseif ($resultCount -ge 2) {
        Write-Host("`nFound $resultCount results:");

        # prints out a list of clients to choose from with visible associated indexes
        for ($i=0; $i -lt $resultCount; $i++) {
            $client = $results[$i];
            $clientName = $client."Full Name";
            $visIndex = $i + 1;
            Write-Host("  $visIndex) $clientName");
        }

        # takes in a single number selection and returns the associated client
        Write-Host("Pick the number corresponding to your desired client, or press q to go back.");
        while ($true) {
            $choicePrompt = Read-Host -Prompt "selection";
            if ($choicePrompt -eq "q") {
                break;
            }
            try {
                if ($choicePrompt/1 -gt 0 -and $choicePrompt/1 -le $resultCount) {
                    return $results[$choicePrompt-1];
                } else { # number isn't an index value
                    Write-Host("[Error] Selection not in range.");
                    continue;
                }
            } catch { # number not recognized
                Write-Host("[Error] Couldn't parse selection");
                continue;
            }
        }
    } else { # no names contained that value, return null
        return $null;
    }
}

# given a name and DOB, finds that client and rejects them
function rejectClient([PSCustomObject]$searchPool, [String]$clientName, [String]$clientDOB) {
    $foundClient = $null;
    foreach ($client in $searchPool) {
        $nameCheck = $client.'Full Name' -eq $clientName;
        $rejectNameCheck = $client.'Full Name' -eq ("*REJECT*" + $clientName);
        $dobCheck = $client.'DOB' -eq $clientDOB;
        if (($nameCheck -or $rejectNameCheck) -and ($dobCheck)) {
            if ($rejectNameCheck) {
                continue;
            } else {
                $foundClient = $client;
                break;
            }
        }
    }

    # builds a new cache list 
    $newCache = @([PSCustomObject]$null);
    if ($null -ne $foundClient) {
        foreach ($client in $searchPool) {
            if ($client -eq $foundClient) {
                $rejectedClient = [PSCustomObject]@{"Full Name"="*REJECT*" + $client."Full Name";"CPhone1"=$client."CPhone1";"caddress"=$client."caddress";"ccity"=$client."ccity";"cstate"=$client."cstate";"czip"=$client."czip";"dob"=$client."dob";"salary"=$client."salary";"Company"=$client."Company"}
                $newCache += $rejectedClient;
            } else {
                $newCache += $client;
            }
        }
        Write-Host("[Delete] Rejected $clientName");
    } else {
        Write-Host("[Error] Couldn't find client to delete.");
    }

    $global:dataCache = $newCache;
    updateMasterCSV;
}

<#
Main Program Loop
#>
initPaths;
while ($true) {
    #Clear-Host;
    Write-Host("-- Menu --`n1) Reports`n2) Phone Directory`n3) Import`n4) Delete`nq) Quit");
    $mainPrompt = Read-Host -Prompt "Selection";
    if (!@("1", "2", "3", "4", "q").Contains($mainPrompt)) {
        continue;
    } else {
        if ($mainPrompt -eq "q") {
            break;
        } else {
            # reads the master CSV to memory every time you pick a menu option
            readMasterToCache;
            if ($mainPrompt -eq "1") {
                runReportBranch;
            } elseif ($mainPrompt -eq "2") {
                runPhoneBranch;
            } elseif ($mainPrompt -eq "3") {
                runImportBranch;
            } elseif ($mainPrompt -eq "4") {
                runDeleteBranch;
            }
        }
    }
}