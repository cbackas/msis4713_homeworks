# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Mod08 PS flow control

$basePrice = 100
$variance = Get-Random -Minimum -15 -Maximum 15
$price = $basePrice + $variance

$payment = 0

Write-Host "Welcome to the soda machine. You can enter values of 5, 10, or 25 as payment."
$userSoda = Read-Host -Prompt "What type of soda would you like?"
Write-Host "The current price of $userSoda is $price cents"

while ($true)
{
    $newPayment = Read-Host -Prompt "Enter a coin"
    $payment += $newPayment/1

    $difference = $price - $payment

    if ($difference -gt 0)
    {
        Write-Host "You still owe $difference cents"
        continue
    }
    elseif ($difference -lt 0)
    {
        $absDifference = [Math]::Abs($difference)
        Write-Host "You have been refunded $absDifference cents"
        break
    }
    else
    {
        break    
    }
}

Write-Host "Enjoy your $userSoda!"