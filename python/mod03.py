#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

import random

list_main = []
list_ints = []

def random_insert(character_name):
    ran_index = random.randint(0, 9)
    list_main.insert(ran_index, character_name)

# count to 10 and get an input each time
# if i wasn't following the guide i would just do while len(list_main) < 10
counter = 0
while counter < 10:
    user_input = input('Please enter a number or a word: ')

    try:
        list_main.append(int(user_input))
    except:
        list_main.append(user_input)

    counter += 1

# check to make sure the list has 10 items
is_full = len(list_main) == 10
print('This list has 10 items. ' + str(is_full), end='\n\n')

# print list
print('This is the list:')
print(list_main, end='\n\n')

# swap first and last item and print
_first = list_main[0]
_last = list_main[9]
list_main[0] = _last
list_main[9] = _first
print('This is the list after swapping the first and last items:')
print(list_main, end='\n\n')

# print first and last 3 in the main list
first_three = [list_main[0], list_main[1], list_main[2]]
last_three = [list_main[7], list_main[8], list_main[9]]
print('These are the first three and last three items in the list:')
print(first_three, end=' ')
print(last_three, end='\n\n')

# print each item on the list
print('These are the individual items in my list:')
for i in list_main:
    print(i)

# check if theres a cat, let the user know
if 'cat' in list_main:
    print('There is a cat in my list.', end='\n\n')
else:
    print('There is not a cat in my list.', end='\n\n')

# prompt for marvel character
input_character = input('Please insert the name of a Marvel character: ')
random_insert(input_character)

# tell the user where the hulk is
print()  # blank line
print(input_character + ' is at index ' + str(list_main.index(input_character)), end='\n\n')

# make a list of only integers
print('These are the integers from the list:')
for i in list_main:
    try:
        list_ints.append(int(i))
    except:
        continue
# sort list_ints - print it
list_ints.sort()
print(list_ints, end='\n\n')

# convert the list (permanently) to a tuple and print it
print('This is the tuple of the list:')
list_main = tuple(list_main)
print(list_main, end='\n\n')

# fail to modify the tuple, catch and print error
try:
    list_main[0] = 'cat'
except:
    print('Tuples are immutable!')
