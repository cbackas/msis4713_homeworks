#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

import sys

concert_dict = {
    'Weezer':
        {'Jan 30, 2018': ['Concrete Gold', 'Etihad Stadium', 'Melbourne, VIC'],
         'Jun 16, 2018': ['Montebello Rockfest 2018', 'Montebello Marina', 'Montebello, QC']},
    'Tenacious D':
        {'May 06, 2018': ['Shaky Knees Music Festival 2018', 'Central Park', 'Atlanta, GA'],
         'Jun 16, 2018': ['Montebello Rockfest 2018', 'Montebello Marina', 'Montebello, QC']},
    'Lamb of God':
        {'Jun 09, 2018': ['Final World Tour: North America 2018', 'Keybank Pavilion', 'Burgettstown, PA'],
         'Jun 16, 2018': ['Montebello Rockfest 2018', 'Montebello Marina', 'Montebello, QC']},
    'Ed Sheeran':
        {'Mar 10, 2018': ['Ed Sheeran with Missy Higgins', 'Etihad Stadium', 'Melbourne, VIC']},
    'Cold War Kids':
        {'Jun 02, 2018': ['XFEST 2018', 'Keybank Pavilion', 'Burgettstown, PA']},
    'Steel Panther':
        {'Oct 21, 2017': ['Aftershock', 'Discovery Park', 'Sacramento, CA']}}


# does the functional part of adding a concert to the main dictionary
def add_concert(concert_band, concert_name, concert_date, concert_venue, concert_location):
    # creates an empty dictionary for a band if there isn't one - so the concert can be added
    if concert_band not in concert_dict.keys():
        concert_dict[concert_band] = {}

    # saves concert information to a date - overrides existing date if there is one
    concert_dict.get(concert_band)[concert_date] = [concert_name, concert_venue, concert_location]


# goes through the concert dictionary and prints the concerts in formatted order
def print_concerts():
    for band in concert_dict.keys():
        print(band)

        dates = concert_dict.get(band)
        for date in dates:
            print('\t' + date)
            lines = dates.get(date)
            for line in lines:
                print('\t\t' + line)
        print('')


# STARTS HERE - prints the existing concerts
print_concerts()

# exists the program if user doesn't opt to add events
input_add_more = input('Enter Y if you would like to add more events: ')
if input_add_more.lower() != 'y':
    sys.exit()
else:
    while True:
        # prompts for concert info and sends it to the add_concert function
        input_1 = input('Artist or Band: ')
        input_2 = input('Concert: ')
        input_3 = input('Date: ')
        input_4 = input('Venue: ')
        input_5 = input('Location: ')
        add_concert(input_1, input_2, input_3, input_4, input_5)

        # if responds with q then it breaks out, else keeps looping
        input_continue = input('q to stop, anything else to continue: ')
        if input_continue.lower() != 'q':
            continue
        else:
            break
