#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019


def math_magic(integer_input):
    return (integer_input**integer_input)+integer_input


while True:
    try:
        user_input = int(input('Please enter an integer between 1 and 10: '))
    except Exception:
        print('That is not an integer. Please try again.')
        continue

    if (user_input > 10) or (user_input < 1):
        print('Integer out of bounds. Please try again.')
    else:
        print(str(math_magic(user_input)))
        end_prompt = input('\nPress Q to exit, anything else to go again: ')

        if end_prompt != 'q':
            continue
        else:
            break
