#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019
# Midterm Project

import phonedir
import os
import sys
import zipfile
import datetime
import shelve
import send2trash
import pprint
import importlib

path_desktop = 'C:\\Users\\Burkman\\Desktop'
path_clientdata = os.path.join(path_desktop, 'Client Data')
path_shelfdata = os.path.join(path_clientdata, 'Data')
path_originalfiles = os.path.join(path_shelfdata, 'Original Files')
path_library = 'C:\\Users\\Burkman\\AppData\\Local\\Programs\\Python\\Python36-32\\Lib\\'

column_def = 'Full Name,CPhone1,caddress,ccity,cstate,czip,dob,salary,Company'

state_codes = ('AK', 'AL', 'AR', 'AS', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'GU', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO',
				'MP', 'MS', 'MT', 'NA', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VI', 'VT', 'WA', 'WI', 'WV', 'WY')
regions = {'New England': ['CT', 'ME', 'MA', 'MH', 'RI', 'VT'], 'Mid-Atlantic': ['NJ', 'NY', 'PA'], 'East North Central': ['IL', 'OH', 'IN', 'WI', 'MI'], 'West North Central': ['ND', 'MN', 'SD', 'NE', 'KS', 'MO', 'IA'], 'Mountain': ['MT',
			'MY', 'CO', 'MN', 'AZ', 'UT', 'NV', 'ID'], 'Pacific': ['WA', 'OR', 'CA'], 'West South Central': ['TX', 'LA', 'AR', 'OK'], 'East South Central': ['MS', 'AL', 'TN', 'KY'], 'South Atlantic': ['FL', 'GA', 'SC', 'NC', 'VA', 'WV', 'MD', 'DE']}

# quick insurance that folders we're gonna need to use exist - runs at app launch
def init_paths():
    paths = [path_desktop, path_clientdata, path_shelfdata, path_originalfiles]
    for p in paths:
        if not os.path.isdir(p):
            os.makedirs(p)


# reads lines from a file given a name and returns a list of strings for each line
def read_lines(file_name):
    _lines = []
    os.chdir(path_desktop)
    with open(file_name) as datafile:
        for line in datafile:
            line = line.rstrip()
            if line != column_def:
                _lines.append(line)
    datafile.close()
    return _lines


# opens a section of the client_data shelf given it's name and returns its data
def read_shelf(shelf_name):
    os.chdir(path_shelfdata)
    shelf = shelve.open('client_data')
    curr_list = []
    try:
        curr_list = shelf[str(shelf_name)]
    except KeyError:
        curr_list = []
        shelf[str(shelf_name)] = curr_list

    shelf.close
    return curr_list


# user selects reports on main menu
def reports_track():
    print('1) List client names, company and salary by company, state or city')
    print('2) Display the client names and salaries who are in the top X% of salaries')
    print('3) Display number of clients in age brackets')
    print('4) Display the average salary by company or by state')
    while True:
        choice = input('Selection> ').lower()
        if choice not in ['1', '2', '3', '4', 'q']:
            continue
        else:
            _curr_clients = read_shelf('clients')
            if _curr_clients == []:
                print(
                    'There are no no clients saved in the db. Please select \'import\' on the main menu.')
                input('press enter to continue...')
                break
            output = ['', '']
            if choice == '1':
                # List client names, company and salary by company, state or city.

                while True:
                    print('Group by what?')
                    list_type = input('company, state, or city> ').lower()
                    if list_type not in ['company', 'state', 'city', 'q']:
                        continue
                    else:
                        if list_type == 'q':
                            sys.exit()
                        else:
                            # makes a list of tuples containing a key and the dictionary as the val
                            sort_type = 'c' + list_type
                            clients = read_shelf('clients')
                            sort_things = []
                            for c in clients:
                                sort_val = c.get(sort_type)
                                name = c.get('cname')
                                sort_val += name  # makes sure that rows are unique so sorting will work
                                sort_things.append((sort_val, c))
                            sort_things.sort()  # sorts the list on the keys

                            # looks through the sorted results and gets the dictionaries out disgarding the sort keys
                            results = []
                            for (key, _dict) in sort_things:
                                results.append(_dict)

                            output[0] = 'clientsby' + list_type
                            output[1] = 'Clients sorted by ' + list_type + '\n'

                            # Collects results into viewable format making sure to only list sort types once if applicable
                            last_group = ''
                            for r in results:
                                name = r.get('cname')
                                company = r.get('ccompany')
                                salary = '$' + r.get('csalary')
                                if sort_type == 'cstate' or sort_type == 'ccity':
                                    curr_group = r.get(sort_type)
                                    if curr_group != last_group:
                                        output[1] += curr_group + '\n'
                                        last_group = curr_group

                                output[1] += '  - ' + name + \
                                    ' (' + company + ') ' + salary + '\n'

                            print_report(output)
                    break

            elif choice == '2':
                # Display the client names and salaries who are in the top X% of salaries.
                # Prompt the user for X% and include that information in your report header.

                while True:
                    print('Show clients in the top ? % of categories:')
                    percentage_prompt = input('Percentage> ').lower()
                    percentage = 0
                    if percentage_prompt == 'q':
                        sys.exit()
                    else:
                        try:
                            # check if the input is actually a number (float)
                            percentage = float(percentage_prompt)
                        except:
                            print('[Error] Error parsing number')
                            continue  # go back and ask again if its not a real number

                        clients = read_shelf('clients')

                        # reads all the clients and finds the biggest salary - to find the baseline
                        salaries = []
                        for c in clients:
                            salary = c.get('csalary')
                            salaries.append(float(salary))
                        top_salary = max(salaries)
                        baseline = top_salary * (percentage/100)

                        # getting necessary info for qualifying clients
                        qualifying_clients = []
                        for c in clients:
                            name = c.get('cname')
                            salary = c.get('csalary')
                            if float(salary) >= baseline:
                                new_dict = {'cname': name, 'csalary': salary}
                                qualifying_clients.append(new_dict)

                        output[0] = 'top'+str(percentage)+'percent'
                        output[1] = 'Clients with top ' + \
                            str(percentage) + '% of salaries:\n'

                        # builds the output
                        for c in qualifying_clients:
                            name = c.get('cname')
                            salary = '$' + c.get('csalary')
                            output[1] += '- ' + name + " " + salary + '\n'

                        print_report(output)
                    break
            elif choice == '3':
                # Display a list of the number of clients who are in the age brackets:
                # younger than 20, between 20 and 30 years old, 30 and 40, 40 and 50, and over 50.

                today = datetime.date.today()

                # ints to hold the count of people in the age brackets
                bracket1 = 0
                bracket2 = 0
                bracket3 = 0
                bracket4 = 0
                bracket5 = 0

                clients = read_shelf('clients')
                for c in clients:
                    str_dob = c.get('cdob')
                    lst_dob = str_dob.split('/')
                    dob = datetime.date(int(lst_dob[2]), int(lst_dob[0]), int(
                        lst_dob[1]))  # create a datetime for their DOB

                    # quick mafs to find out how old they are
                    years = today.year - dob.year
                    if today.month < dob.month or (today.month == dob.month and today.day < dob.day):
                        years -= 1

                    # iterate the appropriate bracket
                    if years < 20:
                        bracket1 += 1
                    elif years >= 20 and years < 30:
                        bracket2 += 1
                    elif years >= 30 and years < 40:
                        bracket3 += 1
                    elif years >= 40 and years < 50:
                        bracket4 += 1
                    elif years >= 50:
                        bracket5 += 1

                # build output
                str_today = today.strftime('%Y-%m-%d')
                output[0] = 'agebrackets' + str_today

                output[1] = 'Age Brackets \n'
                output[1] += 'Under 20: ' + str(bracket1) + '\n'
                output[1] += '20 to 30: ' + str(bracket2) + '\n'
                output[1] += '30 to 40: ' + str(bracket3) + '\n'
                output[1] += '40 to 50: ' + str(bracket4) + '\n'
                output[1] += '50 plus:  ' + str(bracket5) + '\n'

                print_report(output)

            elif choice == '4':
                # Display the average salary by company, by state, or by both (each state by company)

                while True:
                    print('What do you want to group average salary by?')
                    average_prompt = input('Company, state, or both> ').lower()
                    if average_prompt not in ['company', 'state', 'both', 'q']:
                        print('[Error] Input not recognized')
                        continue
                    else:
                        if average_prompt == 'q':
                            sys.exit()

                    clients = read_shelf('clients')

                    # reads all the clients and stores their info into various dictionaries that were constructed to be averaged
                    salaries_by_state = {}
                    salaries_by_company = {}
                    salaries_by_both = {}
                    for c in clients:
                        state = c.get('cstate')
                        company = c.get('ccompany')
                        salary = float(c.get('csalary'))

                        state_salary = salaries_by_state.get(state, [0.0, 0])
                        state_salary[0] += salary
                        state_salary[1] += 1

                        company_salary = salaries_by_company.get(
                            company, [0.0, 0])
                        company_salary[0] += salary
                        company_salary[1] += 1

                        # allows for easily averaging the state and a company's salaries within that state
                        # salaries_by_both = {state: {company: [100.00, 5]}}
                        both_salary = salaries_by_both.get(state, {})
                        company_layer = both_salary.get(company, [0.0, 0])
                        company_layer[0] += salary
                        company_layer[1] += 1
                        both_salary[company] = company_layer

                        salaries_by_state[state] = state_salary
                        salaries_by_company[company] = company_salary
                        salaries_by_both[state] = both_salary

                    # at this point i've already collected the info to do the averaging on the lists if needed
                    if average_prompt == 'state':
                        for state in salaries_by_state:
                            compound = salaries_by_state.get(state)
                            total_sal = compound[0]
                            total_ent = compound[1]
                            average_sal = total_sal / total_ent
                            salaries_by_state[state] = average_sal
                    if average_prompt == 'company':
                        for company in salaries_by_company:
                            compound = salaries_by_company.get(company)
                            total_sal = compound[0]
                            total_ent = compound[1]
                            average_sal = total_sal / total_ent
                            salaries_by_company[company] = average_sal
                    # calculates the average of each company within a state and the states total average at the same time
                    if average_prompt == 'both':
                        for state in salaries_by_both:
                            state_dict = salaries_by_both.get(state)
                            state_total = 0.0
                            state_count = 0
                            for company in state_dict:
                                if state != company:
                                    compound = state_dict.get(company)
                                    total_sal = compound[0]
                                    total_ent = compound[1]
                                    state_total += total_sal
                                    state_count += total_ent
                                    average_sal = total_sal / total_ent
                                    salaries_by_both[state][company] = average_sal
                            salaries_by_both[state][state] = state_total / \
                                state_count

                    # reads the output of the calculations and formats them for output
                    if average_prompt == 'state':
                        output[0] = 'averagesalbystate'
                        output[1] = 'Average Salary by State:\n'
                        for state in salaries_by_state:
                            state_average = "%0.2f" % salaries_by_state.get(
                                state)
                            output[1] += state + ' - $' + state_average + '\n'
                    elif average_prompt == 'company':
                        output[0] = 'averagesalbycompany'
                        output[1] = 'Average Salary by Company:\n'
                        for company in salaries_by_company:
                            company_average = "%0.2f" % salaries_by_company.get(
                                company)
                            output[1] += company + ' - $' + \
                                company_average + '\n'
                    elif average_prompt == 'both':
                        output[0] = 'averagesalbystatecompany'
                        output[1] = 'Average Salary by State then Company:\n'
                        for state in salaries_by_both:
                            companies = salaries_by_both.get(state)
                            state_average = "%0.2f" % companies.get(state)
                            output[1] += state + ' ($' + state_average + ')\n'
                            for company in companies:
                                if company != state:
                                    company_average = "%0.2f" % companies.get(
                                        company)
                                    output[1] += '   - ' + company + \
                                        ': $' + company_average + '\n'

                    print_report(output)
                    break
            elif choice == 'q':
                sys.exit()
        break


# all reports end here - handles how the report content should be printed to the user
def print_report(output_thing):
    title = output_thing[0]
    content = output_thing[1]
    print('Print to screen, file, or both?')
    while True:
        output_prompt = input('screen, file, both> ').lower()
        if output_prompt not in ['screen', 'file', 'both', 'q']:
            continue
        else:
            # lets the user decide how they want their data provided
            if output_prompt == 'file' or output_prompt == 'both':
                os.chdir(path_clientdata)
                if os.path.isfile(title + '.txt'):
                    while True:
                        print(title + '.txt already exists. Overwrite?')
                        file_exists_prompt = input('y/n>')
                        if file_exists_prompt not in ['y', 'n', 'q']:
                            continue
                        else:
                            # handle already existing files with a prompt
                            if file_exists_prompt == 'q':
                                sys.exit()
                            elif file_exists_prompt == 'y':
                                try:
                                    new_file = open(title + '.txt', 'w')
                                    new_file.write(content)
                                    new_file.close()
                                    print(
                                        '[Output] Report output saved to ' + title + '.txt')
                                except:
                                    print(
                                        '[Error] Error saving output to ' + title + '.txt')
                            elif file_exists_prompt == 'n':
                                print('Aborting file overwrite.')
                        break
                else:
                    try:
                        # saving the file
                        new_file = open(title + '.txt', 'w')
                        new_file.write(content)
                        new_file.close()
                        print('[Output] Report output saved to ' + title + '.txt')
                    except:
                        print('[Error] Error saving output to ' + title + '.txt')
            if output_prompt == 'screen' or output_prompt == 'both':
                print()
                print(content)
            print()
            input('press enter to continue...')
            break

# user selects phone directory on main menu


def phone_track():
    print('1) Lookup a specific person')
    print('2) All in a specific state')
    print('3) All in a specific company')
    print('4) All in a speficic region')
    while True:
        choice = input('Selection> ').lower()
        if choice not in ['1', '2', '3', '4', 'q']:
            continue
        else:
            if choice == 'q':
                sys.exit()

            # if theres no phonedir data to report on - then sends user back to menu
            importlib.reload(phonedir)
            _curr_clients = phonedir.clients
            if _curr_clients == {}:
                print(
                    'There are no no clients saved in the phonedir. Please select \'import\' on the main menu.')
                input('press enter to continue...')
                break
            elif choice == '1':
                # search a user with a search term
                while True:
                    print('Enter your search for client name: (\'quit\' to cancel)')
                    name_search = input('Search> ').lower()

                    if name_search == 'quit':
                        break

                    result = search_clients(name_search)
                    if result == None:
                        print(
                            'No clients were found with the search term: \'' + name_search + '\'')
                        continue
                    else:
                        # pull info out of the search result and print it
                        name = result.get('cname')
                        company = result.get('ccompany')
                        phone = result.get('cphone')
                        address = result.get('caddress') + ', ' + result.get(
                            'ccity') + ', ' + result.get('cstate') + ' ' + result.get('czip')
                        print()
                        print(name)
                        print(company)
                        print(phone)
                        print(address)
                        break
            elif choice == '2':
                # Show all clients in a specific state
                print('Show all clients in what state? (state code)')
                while True:
                    state_prompt = input('State> ').upper()
                    if state_prompt not in state_codes or state_prompt == 'q':
                        if state_prompt == 'q':
                            sys.exit()
                        print('[Error] State not found. Try again.')
                        continue
                    else:
                        importlib.reload(phonedir)
                        empty = True
                        for found_client in phonedir.clients.values():
                            client_state = found_client.get('cstate').upper()
                            if client_state == state_prompt:
                                name = found_client.get('cname')
                                company = found_client.get('ccompany')
                                phone = found_client.get('cphone')
                                address = found_client.get('caddress') + ', ' + found_client.get(
                                    'ccity') + ', ' + found_client.get('cstate') + ' ' + found_client.get('czip')

                                print()
                                print(name)
                                print(company)
                                print(phone)
                                print(address)

                                empty = False
                        if empty:
                            print(
                                '[Phonebook] No clients found in ' + state_prompt)
                        break
            elif choice == '3':
                # Show all clients in a specific company

                # builds a list of existing companies so matching works and the user can 'listall'
                importlib.reload(phonedir)
                clients = phonedir.clients.values()
                companies = []
                for client in clients:
                    comp = client.get('ccompany').lower()
                    if comp not in companies:
                        companies.append(comp)

                print('Show all clients in what company? (or \'listall\')')
                while True:
                    company_prompt = input('Company> ').lower()
                    if company_prompt not in companies or company_prompt == 'q' or company_prompt == 'listall':
                        if company_prompt == 'q':
                            sys.exit()
                        # allows the user to see avalable companies
                        if company_prompt == 'listall':
                            print()
                            print('Stored Companies: ')
                            for c in companies:
                                print(c.title())
                            continue
                        print(
                            '[Error] Company not found. Try again or respond \'listall\' to list all companies.')
                        continue
                    else:
                        print()
                        print('Clients in ' + company_prompt.title() + ':')
                        for found_client in clients:
                            client_company = found_client.get('ccompany')
                            if client_company.lower() == company_prompt:
                                name = found_client.get('cname')
                                phone = found_client.get('cphone')
                                address = found_client.get('caddress') + ', ' + found_client.get(
                                    'ccity') + ', ' + found_client.get('cstate') + ' ' + found_client.get('czip')
                                print()
                                print(name)
                                print(client_company)
                                print(phone)
                                print(address)
                        break
            elif choice == '4':
                # show all clients in a specific region
                print('Show all clients in what region? (or \'listall\')')
                # builds a region list so we can make sure the user input is in that list
                region_list = []
                for r in regions.keys():
                    region_list.append(r.lower())
                while True:
                    region_prompt = input('Region> ').lower()
                    if region_prompt not in region_list or region_prompt == 'q' or region_prompt == 'listall':
                        if region_prompt == 'q':
                            sys.exit()
                        # allows the user to see available regions
                        if region_prompt == 'listall':
                            print()
                            print('US Regions:')
                            for r in regions.keys():
                                print(r)
                            continue
                        print(
                            '[Error] regions not found. Try again or respond \'listall\' to list all regions.')
                        continue
                    else:
                        # once we know the regions, we get all the states that are in that region
                        qualifying_states = []
                        for key in regions:
                            if key.lower() == region_prompt:
                                qualifying_states = regions.get(key)
                                break

                        print()
                        print('Clients in ' + region_prompt.title() + ' Region:')

                        clients = read_shelf('clients')
                        empty = True  # flag to make sure we print something even if no clients found
                        # loop and print the clients who are in qualifying states
                        for found_client in clients:
                            candidate_state = found_client.get('cstate')
                            if candidate_state in qualifying_states:
                                name = found_client.get('cname')
                                company = found_client.get('ccompany')
                                phone = found_client.get('cphone')
                                address = found_client.get('caddress') + ', ' + found_client.get(
                                    'ccity') + ', ' + found_client.get('cstate') + ' ' + found_client.get('czip')
                                print()
                                print(name)
                                print(company)
                                print(phone)
                                print(address)
                                empty = False
                        if empty:
                            print(
                                '[Phonebook] No clients found in ' + region_prompt)
                        break
            break


# search procedure - returns dictionary of the client we want (from phonedir)
def search_clients(user_input):
    importlib.reload(phonedir)
    user_input = user_input.lower()

    # look through all the clients in phone dir and return ones that contain the search term
    results = []
    for (key, _dict) in phonedir.clients.items():
        contestant_name = key.lower()
        if user_input in contestant_name:
            results.append(_dict)

    r_count = len(results)
    print(results)
    if r_count == 1:
        return results[0]
    elif r_count >= 2:
        print('Found ' + str(r_count) + ' results:')

        # rough key value pair to match names and their shown number on the list below
        indexer = {}
        last_index = 1
        for client in results:
            name = client.get('cname')
            print('  ' + str(last_index) + ') ' + name)
            indexer[last_index] = client
            last_index += 1

        # prompts the user to select a client by their corresponding number
        # does the partial matching
        print('Which client do you want to see?')
        specific_person = input('Selection> ').lower()
        keys = []
        for k in indexer.keys():
            keys.append(str(k))
        if specific_person in keys or specific_person == 'q':
            if specific_person == 'q':
                sys.exit()
            # returns the dictionary of the selected client
            return indexer.get(int(specific_person))
    else:
        return None


# goes through all the existing clients in the shelf and makes the phonebook line up with it
def phonebook_sync_clients():
    importlib.reload(phonedir)
    clients = read_shelf('clients')
    pb_dict = {}
    for c in clients:
        name = c.get('cname')
        phone = c.get('cphone')
        address = c.get('caddress')
        city = c.get('ccity')
        state = c.get('cstate')
        zipcode = c.get('czip')
        company = c.get('ccompany')
        dob = c.get('cdob')
        shrunk_dict = {'cname': name, 'cphone': phone, 'caddress': address, 'ccity': city,
                       'cstate': state, 'czip': zipcode, 'ccompany': company, 'cdob': dob}
        pb_dict[name] = shrunk_dict
    write_phone_module(pb_dict)


# makes sure the phonedir.py module exists, in an empty form at least
def init_pb_module():
    os.chdir(path_library)
    if not os.path.isfile('phonedir.py'):
        write_phone_module({})

# writes a pprint form of whatever (dictionary) is passed to it to the phonedir module


def write_phone_module(stuff):
    os.chdir(path_library)
    phonedir_file = open('phonedir.py', 'w')
    phonedir_file.write('clients = ' + pprint.pformat(stuff) + '\n')
    phonedir_file.close()

# user selects import on main menu


def import_track():
    while True:
        print('Enter the name and extension of source data:')
        input_filename = input('Filename> ').lower()
        if input_filename == 'q':
            sys.exit()

        # make sure user is putting in real args
        os.chdir(path_desktop)
        if input_filename.endswith('.csv'):
            if os.path.isfile(input_filename):
                lines = read_lines(input_filename)
                shelve_dict(lines)
                archive_csv(input_filename)
                phonebook_sync_clients()  # rebuilds the phonebook py file to match shelf info
            else:
                print('File not found.')
                continue
        else:
            print('That\'s not a .csv file')
            continue

        # make sure they only imput y or n and keeps asking them if they don't
        go_again = ''
        while True:
            print('Import another file?')
            go_again = input('y/n> ').lower()
            if go_again in ['y', 'n', 'q']:
                break
            else:
                print('[Error] y or n required')
                continue
        if go_again == 'q':
            sys.exit()
        elif go_again == 'n':  # by this point go_again has to equal either n or y
            break


# takes in a list of lines, converts them to a list of dicts and appends them to the shelf (ignoring duplicates or rejects)
def shelve_dict(lines_input):
    _list = []
    for line in lines_input:
        cols = line.split(',')
        new_dict = {'cname': cols[0], 'cphone': cols[1], 'caddress': cols[2], 'ccity': cols[3], 'cstate': cols[4],
                    'czip': cols[5], 'cdob': cols[6], 'csalary': cols[7], 'ccompany': cols[8]}
        _list.append(new_dict)

    os.chdir(path_shelfdata)
    shelf = shelve.open('client_data')
    new_list = []
    try:
        curr_list_clients = read_shelf('clients')
        new_list = curr_list_clients.copy()
    except KeyError:
        pass
        # tries to get all the exsting data if it exists and just idk doesn't the shelf doesn't exist yet

    # gets all the rejected things to check against and passes through to already stored
    rejected_list = read_shelf('rejected')
    for client in _list:
        name = client.get('cname')
        if already_stored(client, new_list):
            print('[Import] Skipping ' + name + ' (already have)')
            continue
        elif already_stored(client, rejected_list):
            print('[Import] Skipping ' + name + ' (rejected)')
        else:
            new_list.append(client)
            print('[Import] Importing ' + name)
            continue

    shelf['clients'] = new_list
    shelf.close()


# just adds a specificly given dictionary to the reject list
def reject_client(name, dob):
    # rebuilds the client list to exclude the rejected client
    curr_list_clients = read_shelf('clients')
    new_list_clients = []
    for c in curr_list_clients:
        cname = c.get('cname')
        cdob = c.get('cdob')
        if name != cname and dob != cdob:
            new_list_clients.append(c)

    # adds the now rejected client to the rejected area of the shelf
    curr_list_rejected = read_shelf('rejected')
    new_list_rejected = curr_list_rejected.copy()
    new_dict = {'cname': name, 'cdob': dob}
    new_list_rejected.append(new_dict)

    # writes the new data to the shelf
    os.chdir(path_shelfdata)
    shelf = shelve.open('client_data')
    shelf['clients'] = new_list_clients
    shelf['rejected'] = new_list_rejected
    shelf.close()
    print('[Delete] Moved ' + name +
          ' to the rejected list and deleted their info from the phone directory.')
    phonebook_sync_clients()  # makes the phonedir match the newly reject-free client list


# quick basic bool check for if a dictionary is in a nother list of dictionaries
def already_stored(input_client_dict, list_dict):
    input_name = input_client_dict.get('cname')
    input_dob = input_client_dict.get('cdob')
    for _dict in list_dict:
        _dict_name = _dict.get('cname')
        _dict_dob = _dict.get('cdob')
        if _dict_name == input_name and _dict_dob == input_dob:
            return True
    return False

# copies a given file to a zip file and deletes the original


def archive_csv(file_name):
    zip_name = file_name.replace('.csv', '.zip')
    os.chdir(path_originalfiles)
    zip_file = zipfile.ZipFile(zip_name, 'w')
    try:
        os.chdir(path_desktop)
        zip_file.write(file_name, compress_type=zipfile.ZIP_DEFLATED)
        send2trash.send2trash(file_name)
        print('Successfully imported and archived ' + file_name)
    except:
        print('Error archiving ' + file_name + ' to ' + zip_name)
    zip_file.close()


# user selects delete on main menu
def delete_track():
    _curr_clients = read_shelf('clients')
    if _curr_clients == []:
        print('There are no no clients saved in the db. Please select \'import\' on the main menu.')
        input('press enter to continue...')
        return  # return our way out of there if theres no clients to delete
    print('To delete a client, enter your search for a client name:')
    while True:
        name_search = input('Search> ')
        if name_search == 'quit':
            break
        result = search_clients(name_search)
        if result == None:
            print('No clients were found with the search term: \'' +
                  name_search + '\'')
            continue
        else:
            # prompt to make sure they're deleting who they want to delete and do it
            name = result.get('cname')
            dob = result.get('cdob')
            print('[Delete] Are you sure you\'d like to delete ' + name + '?')
            while True:
                delete_prompt = input('y/n> ').lower()
                if delete_prompt not in ['y', 'n', 'q']:
                    continue
                else:
                    if delete_prompt == 'q':  # always give a way to close
                        sys.exit()
                    elif delete_prompt == 'y':
                        reject_client(name, dob)
                    elif delete_prompt == 'n':
                        print('[Delete] Aborted delete of ' + name)
                    break
            break


# -- PROGRAM STARTS HERE --
# prints main menu and takes you off on paths depending on choice,
# always bringing you back here for more fun
init_paths()
init_pb_module()
while True:
    print()
    print('-- Menu --')
    print('1) Reports')
    print('2) Phone Directory')
    print('3) Import')
    print('4) Delete')
    print('q) Quit (enter q at any prompt to close program)')
    print('Enter the number that corrisponds with your selection.')
    prompt = input('Selection> ').lower()
    if prompt not in ['1', '2', '3', '4', 'q']:
        continue
    else:
        if prompt == '1':
            print()
            reports_track()
        elif prompt == '2':
            print()
            phone_track()
        elif prompt == '3':
            print()
            import_track()
        elif prompt == '4':
            print()
            delete_track()
        elif prompt == 'q':
            sys.exit()
