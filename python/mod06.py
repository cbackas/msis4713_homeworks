#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

import os
import shelve
import pprint
import state_data

path_data = 'C:\\Users\\Burkman\\Desktop\\'
path_py_library = 'C:\\Users\\Burkman\\AppData\\Local\\Programs\\Python\\Python36-32\\Lib\\'

dict_states = {}
state_list = []


# quick function to shelve data (shelves everything inside dict_states at the time of execution
def shelve_data():
    if os.path.isfile('state_data'):
        os.remove('state_data.dat')
        os.remove('state_data.dir')

    shelf = shelve.open('state_data')
    shelf['state_info'] = dict_states
    shelf.close()


# parses columns out, creates a dictionary with the info needed, sends the dictionary back
def create_dict(_cols):
    # extracting vars
    state_name = _cols[3]
    total_returns = int(_cols[4])
    total_exemptions = int(_cols[5])
    exemptions_per_return = total_exemptions / total_returns
    income_dividend = int(_cols[8])
    income_interest = int(_cols[9])
    income_adjustedgross = int(_cols[6])
    income_capital = (income_dividend + income_interest) / income_adjustedgross

    # make a new dictionary entry for the state and add it to the state list
    _dict = {state_name: [exemptions_per_return, income_capital]}
    return _dict


# STARTS HERE
# opens csv file, checks if a row is a state and saves that row to the shelf
os.chdir(path_data)
with open('Mod 06 Python Paths and File IO Income Data.csv') as lines_csv:
    for line in lines_csv:
        cols = line.split(',')
        state = cols[3]
        if state.isupper():  # all caps means its a state row
            dict_states[state] = line
            shelve_data()
lines_csv.close()

# opens the shelf and reads each (state) row to make a couple simple calculations - then adds to a list of dictionaries
shelf_file = shelve.open('state_data')
for key in shelf_file['state_info'].keys():
    state_line = shelf_file['state_info'][key]
    cols = state_line.split(',')
    new_dict = create_dict(cols)
    state_list.append(new_dict)
shelf_file.close()

# opens a file in your python library and saves the state_list output to the module
os.chdir(path_py_library)
state_data_module = open('state_data.py', 'w')
state_data_module.write('state_data = ' + pprint.pformat(state_list) + '\n')
state_data_module.close()

# reads from the state_data module, creating sum and average vars
sum_exemptions_per_return = 0
sum_income_capital = 0
for list_state_dicts in state_data.state_data:
    for list_vals in list_state_dicts.values():
        sum_exemptions_per_return += list_vals[0]
        sum_income_capital += list_vals[1]
average_exemptions_per_return = sum_exemptions_per_return / len(state_data.state_data)
average_income_capital = sum_income_capital / len(state_data.state_data)

# prints the final output
print('The average number of exceptions per return is: ' + str(round(average_exemptions_per_return, 2)))
print('The average number of capital income to AGI is: ' + str(round(average_income_capital, 2)))
