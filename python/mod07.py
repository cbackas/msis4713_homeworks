#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

import os, shutil, zipfile

# path_desktop = 'C:\\Users\\Burkman\\Desktop\\'
path_desktop = 'G:\\MSIS 4713 (Scripting Essentials)\\Homeworks\\Mod07'
path_py_scripts = os.path.join(path_desktop, 'py_scripts')
alpha_string = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'


def move_to_scripts(path_from):
    init_folder(path_py_scripts)
    base_name = os.path.basename(path_from)
    for letter in alpha_string:  # for each letter in the alpha_string
        if base_name[0].upper() == letter:  # check if the upper case version of our first letter equals that letter
            letter_folder = os.path.join(path_py_scripts, letter)  # .\py_scripts\A
            path_to = os.path.join(letter_folder, base_name)  # .\py_scrupts\A\file_name.ps1
            init_folder(letter_folder)  # makes sure the letter folder exists
            print('[Copying] ' + base_name)
            shutil.copy(path_from, path_to)  # copy
            print('[Zipping] ' + base_name)
            os.chdir(path_py_scripts)
            try:
                zip = zipfile.ZipFile(letter + '.zip', 'a')  # access zip for the current letter
                os.chdir(letter_folder)
                zip.write(base_name, compress_type=zipfile.ZIP_DEFLATED)  # write the current file to the zip
            except UserWarning:
                print('[Zipping] File already in zip')
            zip.close()


# makes given a path, if a folder doesn't exist then make it
def init_folder(path):
    if not os.path.isdir(path):
        os.makedirs(path)


files_found = []
search_ext = '.ps1'
walk = os.walk('C:\\')
for dirpath, subdirs, files in walk:  # for each file in the C: drive walk
    for file in files:
        if file.lower().endswith(search_ext) and file not in files_found:  # if file ends with .ps1 and not used before
            files_found.append(file)
            move_to_scripts(os.path.join(dirpath, file))
print('Done!')
