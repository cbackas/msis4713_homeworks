#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

all_lyrics = ("Quisiera:Ayer:cambiarle:conocí:el:un:final"
               ":cielo:al:sin:cuento|Las:sol|Y:barras:un:y"
               ":hombre:los:sin:tragos:suelo|Un:han:santo:"
               "sido:en:testigo|De:prision|Y:el:una:dolor:"
               "canción:que:triste:me:sin:causaste:dueño|Y:"
               "y:conocí:to':tus:lo:ojos:que:negros|Y:hiciste"
               ":ahora:conmigo|Un:sí:infeliz:que:en:no:el:"
               "puedo:amor,:vivir:que:sin:aun:ellos:no:yo|"
               "Le:te:pido:supera|Que:al:ahora:cielo:camina"
               ":solo:solo:un:sin:deseo|Que:nadie:en:por:tus"
               ":todas:ojos:las:yo:aceras|Preguntándole:pueda"
               ":a:vivir|He:Dios:recorrido:si:el:en:mundo:verdad"
               ":entero|te:el:vengo:amor:a:existe|:decir|")

# list_one/list_two are for storing individual song lyrics, shared_words contains words in both songs
list_one = []
list_two = []
shared_words = []

# converts all_lyrics from a string to a list
list_lyrics = all_lyrics.split(':')

# loops however long the original list is, if its an odd number than it gets put in list_one, if its even list_two
counter = 0
while counter < len(list_lyrics):
    if counter % 2 != 0:
        list_one.append(list_lyrics[counter])
    else:
        list_two.append(list_lyrics[counter])
    counter += 1

# puts spaces and turns it into a string
song_one = ' '.join(list_one)
song_two = ' '.join(list_two)

# prints the two sets of lyrics
print('--- SONG ONE ---')
print(song_one.replace('|', '\n'), end='\n')
print('--- SONG TWO ---')
print(song_two.replace('|', '\n'), end='\n')

# (since both songs are the same length)
# for every word in list_one check to see if its in list_two
# if it is put it in shared_words so it doesn't get added to the list twice, then print the word
print('Words that are in both songs:')
for i in list_one:
    if i in list_two and i not in shared_words:
        shared_words.append(i)
        print(i)
