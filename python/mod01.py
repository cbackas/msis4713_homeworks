#! python3
# Zac Gibson
# MSIS 4713
# OkState Spring 2019

import random

BASE_PRICE = 100
Variance = random.randint(-3,3)*5
Price = BASE_PRICE + Variance

Payment = 0

print('Welcome to the soda machine. You can enter values of 5, 10, or 25 as payment.')
UserSoda = input('What type of soda would you like? ')
print('The current price of ' + UserSoda + ' is ' + str(Price) + ' cents')

while True:
    New_Payment = input('Enter a coin: ')
    Payment += int(New_Payment)

    Difference = Price - Payment

    if Difference > 0:
        print('You still owe ' + str(Difference) + ' cents.')
        continue
    elif Difference < 0:
        print('You have been refunded ' + str(abs(Difference)) + ' cents.')
        break
    else:
        break

print('Enjoy your ' + UserSoda + '!')
